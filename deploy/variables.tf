variable "prefix" {
  type        = string
  default     = "raad"
  description = "prefix for all resources"
}

variable "project" {
  type        = string
  default     = "recip-app-api-devops"
  description = "Name of the project"
}

variable "contact" {
  type        = string
  default     = "tweenlee@gmail.com"
  description = "contac information"
}

variable "db_username" {
  description = "Username for RDS postgress instance"
}

variable "db_password" {
  description = "Password for RDS postgress instance"
}

variable "bastion_key_name" {
  default = "recepi-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "295551168051.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for API Proxy"
  default     = "295551168051.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"

}