terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket         = "recipe-aws-devops-tweenlee"
    key            = "recipe-api.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}

data "aws_region" "current" {}